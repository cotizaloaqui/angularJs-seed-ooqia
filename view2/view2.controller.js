'use strict';

angular.module('view2Module', [
	'view2ServiceModule',
])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2',
    resolve: {
         checkSession : function(sessionService) {
            return sessionService.verifySessionRoute();
         }
      }
  });
}])

.controller('View2', ['$scope', '$timeout', 'view2', 'appMain', function($scope, $timeout, view2, appMain) {
	appMain.init($scope,view2);

	$scope.$on('$viewContentLoaded', function () {
      $scope.featureSrv.init();
   });

   $scope.$on("$destroy", function () {

   });

}]);