'use strict';

angular.module('view2ServiceModule', [])

.service('view2', ['$location', function($location) {
	var that = this;

	this.init = function() {
		console.log('init logic controller 2');
	};

}]);