'use strict';

angular.module('view1Module', [
	'view1ServiceModule',
])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/view1', {
    templateUrl: 'view1/view1.html',
    controller: 'View1',
     resolve: {
         /*checkSession : function(sessionService) {
            return sessionService.verifySessionRoute();
         }*/
      }
  });
}])

.controller('View1', ['$scope', '$timeout', 'view1', 'appMain', function($scope, $timeout, view1, appMain) {
  appMain.init($scope,view1);	

	$scope.$on('$viewContentLoaded', function () {
      $scope.featureSrv.init();
   });

   $scope.$on("$destroy", function () {

   });

}]);