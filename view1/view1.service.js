'use strict';

angular.module('view1ServiceModule', [])

.service('view1', ['$location', 'request', function($location, request) {
	var that = this;

	this.init = function() {
		this.appMain.viewFullHeight = true;
		console.log('init logic controller 1');
	};

}]);