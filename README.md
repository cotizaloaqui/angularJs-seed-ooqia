This structure  follow these 4 basic guidelines. LIFT guidelines

* Locating our code is easy
* Identify code at a glance
* Flat structure as long as we can
* Try to stay DRY (Don’t Repeat Yourself) or T-DRY

Please visit : http://www.johnpapa.net/angular-app-structuring-guidelines/

Components:

* Font awesome 4.30
* toaster 0.4.5
* Angular Facebook
* Google maps 
* angular Bootstrap UI  0.13.3 
* AngularJs 1.4.3
* chance.js
* colpick.js 2013
* SCEditor 1.4.7 
* Jquery 1.11.3
* Less 2.5.1
* MarkerCluster.js
* Oms.js
* Underscore.js 1.8.3
* animate.css