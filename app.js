'use strict';

var mainApp = angular.module('myApp', [
	'ngRoute',
	'ngCookies',
	'ngSanitize',
	'ngResource',
	'appMainModule',
	'commonDirectivesModule',
	'authenticationModule',
	'view1Module',
	'view2Module',
	'loginModule',
	'ngAnimate',
	'ui.bootstrap',
	'toaster',
	'facebook',
	'googleMaps'
	])

// Main app's constants
.constant('ENV_REST', {
	// UAT
	/*wanderingAPIUrl: 'http://uat.thewandering.net/api/public/api',
	requestUrl: 'http://' + window.location.host + '/php/php.php',
	loginCrossDomain: 'http://uat.thewandering.net/site/wander/php/loginCrossDomain.php',
	checkSessionWanderingUrl: 'http://uat.thewandering.net/site/wander/php/isUserLogged.php'*/

	// PROD
	//wanderingAPIUrl: 'http://thewandering.net/rest/public/api',
	//wanderingUrlCreateStation: 'http://thewandering.net/diy/index.php',
	//requestUrl: 'http://' + window.location.host + '/php/php.php',
	//loginCrossDomain: 'http://thewandering.net/site/wander/php/loginCrossDomain.php',
	//checkSessionWanderingUrl: 'http://thewandering.net/site/wander/php/isUserLogged.php'
})

.config(['$routeProvider', function ($routeProvider) {
 $routeProvider.otherwise({ redirectTo: '/' });
}])

.config(['FacebookProvider', function (FacebookProvider) {
	/*facebook ooqia with saul.burgos@ooqia.com*/
	FacebookProvider.init('540072492791261');
}]);