'use strict';

// General app directives
angular.module('commonDirectivesModule', [])

.directive('ajaxLoading', ['appMain', function (appMain) {

	return {
		restrict: 'E',
		template: '<div id="loadingAjax" ng-show="appMain.showLoadingAjax">' +
						'<img src="components/images/ajax-loader.gif" />' +
					'</div>',
		replace: true,
		scope: {},
		link: function ($scope, element, attrs) {
			// showLoadingAjax value managed in appMain service
			$scope.appMain = appMain;

			//create a event
			/*$scope.$on('startAjax', function () {
				appMain.showLoadingAjax = true;
			});

			//create a event
			$scope.$on('stopAjax', function () {
				appMain.showLoadingAjax = false;
			});*/
		}
	};

}])

.directive("widgetSlidePanel", ['version', function (version) {

   return {
      restrict: 'A',
      link: function ($scope, element, attrs) {

         var bodyWidth = 180;
         var widthOnHidden = 0;
         var DESKTOP_WIDTH = 0.3;
         var MOBILE_WIDTH = 0.9;

         function slideTogglePanelSides(currentElement, show) {

            bodyWidth = parseInt(angular.element('body').css('width'), 10);

            if (bodyWidth < 640) {
               bodyWidth = bodyWidth * MOBILE_WIDTH;
            }

            if (bodyWidth >= 641 && bodyWidth <= 1024) {
               bodyWidth = bodyWidth * DESKTOP_WIDTH;
            }

            if (bodyWidth >= 1025 && bodyWidth <= 1440) {
               bodyWidth = bodyWidth * DESKTOP_WIDTH;
            }

            if (bodyWidth >= 1440) {
               bodyWidth = bodyWidth * DESKTOP_WIDTH;
            }

            if (show) {
               currentElement.show();
               currentElement.animate({
                  opacity: 1,
                  width: bodyWidth
               }, "fast");
            } else {
               currentElement.animate({
                  opacity: 1,
                  width: widthOnHidden
               }, "fast", function () {
                  jQuery(this).hide();
               });
            }
         }

         function slideTogglePanelUpDown(currentElement, show) {

            switch (attrs.widgetSlidePanelType) {
               case 'desktop':
                  var bodyHeight = parseInt(angular.element('body').css('height'), 10);
                  bodyHeight = bodyHeight * 0.8;
                  widthOnHidden = 0;
                  break;
               case 'mobile':
                  var bodyHeight = parseInt(angular.element('body').css('height'), 10);
                  widthOnHidden = 0;
                  bodyHeight = attrs.widgetSlidePanelHeight ? bodyHeight * attrs.widgetSlidePanelHeight : bodyHeight * 0.8;
                  break;
            }

            if (show) {
               currentElement.animate({
                  opacity: 1,
                  height: bodyHeight
               }, "fast");
            } else {
               currentElement.animate({
                  opacity: 0,
                  height: widthOnHidden
               }, "fast");
            }
         }

         switch (attrs.widgetSlidePanel) {
            case 'right':
               $scope.$watch('appMain.slidePanel.showRight', function (newValue, oldValue) {
                  slideTogglePanelSides(element, newValue);
               });
               break;
            case 'left':
               $scope.$watch('appMain.slidePanel.showLeft', function (newValue, oldValue) {
                  slideTogglePanelSides(element, newValue);
               });
               break;
            case 'bottom':
               $scope.$watch('appMain.slidePanel.showBottom', function (newValue, oldValue) {
                  slideTogglePanelUpDown(element, newValue);
               });
               break;
            case 'top':
               $scope.$watch('appMain.slidePanel.showTop', function (newValue, oldValue) {
                  slideTogglePanelUpDown(element, newValue);
               });
               break;
         }
      }
   };

}])

.directive('onErrorImage', function () {
	// Runs during compile
	return {
		restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
		link: function ($scope, iElm, iAttrs, controller) {

			iElm.error(function () {
				jQuery(this).attr('src', 'http://placehold.it/200x200&text=No+Image');
			});

		}
	};
})

.directive('imageResponsive', function () {
   // Runs during compile
   return {
      restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
      link: function ($scope, iElm, iAttrs, controller) {

         iElm.on("load", function() {
            var height = jQuery(this).height();
            var width = jQuery(this).width();
            
            if (width > height) {
               //it's a landscape
               jQuery(this).addClass('responsiveImage--landscape');
               //prevent error in some image
               if(jQuery(this).parent().outerHeight() < height) {
                  jQuery(this).removeClass('responsiveImage--landscape');   
                  jQuery(this).addClass('responsiveImage--portrait');
               }

            } else if (width < height){
               //it's a portrait
               jQuery(this).addClass('responsiveImage--portrait');
            } else {
               //image width and height are equal, therefore it is square.
               jQuery(this).addClass('responsiveImage--portrait');
            }

         });        

      }
   };
})

// Used to test values in forms
.directive('fakeData', function () {
	// Runs during compile
	return {
		restrict: 'A', // E = Element, A = Attribute, C = Class, M = Comment
		require: 'ngModel',
		scope: false,
		link: function ($scope, iElm, iAttrs, ngModel) {
			ngModel.$setViewValue(chance[iAttrs.fakeData]());
			ngModel.$render();
		}
	};
})

// Directive for clean string and remove break lines for inputs and textareas
// only add directive in element and set a model
.directive("cleanString", function() {
	return {
		require: 'ngModel',
		link: function(scope, element, attrs, ngModelController) {

			// Clean string for breaklines and scape symbols
			var getcleanString = function(jsonString, removeBreakLine) {
				removeBreakLine = removeBreakLine || true;

				if (jsonString === '' || _.isNull(jsonString)) {
					return jsonString;
				}

				if (removeBreakLine) {
					var match = /\r|\n/.exec(jsonString);

					if (match) {
						jsonString = jsonString.replace(/(?:\r\n|\r|\n)/g, ' ');
					}
				}

				jsonString = jsonString.replace(/\\/g, '');

				return _.escape(jsonString);
			};

			// Clean string for breaklines and escape symbols
			var setcleanString = function(jsonString) {
				var match = /\r|\n/.exec(jsonString);

				if (jsonString === '' || _.isNull(jsonString)) {
					return jsonString;
				}

				if (match) {
					jsonString = jsonString.replace(/(?:\r\n|\r|\n)/g, ' ');
				}

				return _.unescape(jsonString);
			};

			// Convert data from view format to model
			// return data.toLowerCase(); //converted
			ngModelController.$parsers.push(function(data) {
				return getcleanString(data);
			});

			ngModelController.$formatters.push(function(data) {
			  // Convert data from model format to view
			  // return data.toLowerCase(); //converted
			  return setcleanString(data);
			});
		}
	};
})

.directive('pickerColor', function () {
   return {
      restrict: "A",
      scope: {
         pickerValue: '=',
      },
      link: function (scope, element, attrs) {

         jQuery(element).colpick({
            color: scope.pickerValue || '#2f72b5',
            submit: false,
            onChange: function (hsb, hex, rgb, el, bySetColor) {
               jQuery(el).css('background-color', '#' + hex);
               scope.pickerValue = '#' + hex;

               // Fill the text box just if the color was set using the picker, and not the colpickSetColor function.
               if (!bySetColor) jQuery(el).val(hex);
               // Apply values on angular model
               scope.$apply();
            }
         })
         .css({ 'background-color': scope.pickerValue, cursor: 'pointer' })
         .attr('readonly', 'readonly');

         scope.$watch('pickerValue', function (newValue) {
            if (newValue) {
               jQuery(element).css('background-color', scope.pickerValue);
            } else {
               jQuery(element).css('background-color', '#fff');
            }
         });
      }
   };
})

.directive('sceditor', function () {
	return {
		restrict: "A",
		scope: {
			sceditorModel: '='
		},
		link: function (scope, element, attrs) {

			jQuery(element).sceditor({
				height: 350,
				resizeEnabled: false,
				emoticonsEnabled: false,
				width: '100%',
				toolbar: 'bold,italic,underline,strike|left,center,right,justify|font,size,color,removeformat|bulletlist,orderedlist,indent,outdent|quote,horizontalrule|image,email,link,unlink|date,time|ltr,rtl|maximize,source'
			});

			// Set model when is created at first time
			jQuery(element).sceditor('instance').val(scope.sceditorModel);

			var updateModel = function () {
				scope.sceditorModel = jQuery(element).sceditor('instance').val();
				scope.$apply();
			};

			// Events for update angular model
			jQuery(element).sceditor('instance').keyUp(function (e) {
				updateModel();
			});

			jQuery(element).sceditor('instance').nodeChanged(function (e) {
				updateModel();
			});

			jQuery(element).sceditor('instance').blur(function (e) {
				updateModel();
			});
		}
	};
});