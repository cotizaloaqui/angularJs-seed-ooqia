﻿'use strict';

angular.module('authenticationModule', [])

.service('authRestService', function( $resource ,$q,toaster,ENV_REST) {
	var that = this;

	this.config = {
		urlRest: HOSTSNAMES.WANDERING_API,
		errorHandler : function(err) {
			console.log(err);
		},
		startLoadingIndicator : undefined,
		stopLoadingIndicator : undefined
	};

	var loginCustom = {
		method:'POST',
		params: null,
		isArray: false,
		/*transformRequest : function(data, headersGetter) {

		},*/
      interceptor: {
       	response: function (data) {
           	//console.log('response in interceptor', data);
           	return data;
       	},
       	responseError: function (responData) {
          	switch(responData.status) {
					case 400:
					case 401:
					case 422:
				  		toaster.pop("error",'', responData.data.Message, 5000);
				  	break;
				}
          	return $q.reject(responData);
       	}
      }
	};

	var registerCustom = {
		url : this.config.urlRest + '/users',
		method:'POST',
		params: null,
		isArray:false,
      interceptor: {
       	response: function (data) {
           	return data;
       	},
       	responseError: function (responData) {
       		switch(responData.status) {
					case 422:
				  		toaster.pop("error",'Info', responData.data.message, 5000);
				  	break;
				}
          	return $q.reject(responData);
       	}
      }
	};



	this.resource =  $resource(this.config.urlRest + '/login', null,{
		'login': loginCustom ,
		'register' : registerCustom
	});

})

.service('sessionService', function($q, $location,$cookies) {

	var sessionMode = 'cookies';
	var nameData = "experiencityUserSession";

	this.create = function(data) {
		if(typeof(Storage) !== "undefined" ) {

			if(sessionMode == 'cookies') {
				$cookies.put(nameData, angular.toJson(data));
			} else {
				localStorage.setItem(nameData,angular.toJson(data));
			}
		}
	};

	this.destroy = function() {
		if(sessionMode == 'cookies') {
			delete $cookies.remove(nameData);
		} else {
			localStorage.removeItem(nameData);
		}
	};

	this.getSession = function() {

		if(sessionMode == 'cookies') {
		   try {
		      if (typeof $cookies.get(nameData) !== 'undefined') {
		         return angular.fromJson($cookies.get(nameData));
		      } else {
		         return {};
		      }
		   } catch(e) {
		      //session data is a string separate by commas (old way)
		      if (typeof $cookies.get(nameData) !== 'undefined') {

		         var userData = $cookies.get(nameData).split(',');
		         return {
		            name: userData[0].replace('+',' '),
		            email: userData[2],
		            id: userData[3],
		            userLevel: userData[4]
		         }

		      } else {
		         return {};
		      }
		   }

		} else {

			if (localStorage.getItem(nameData) !== null) {
				return angular.fromJson(localStorage.getItem(nameData));
			} else {
				return {};
			}
		}

	};

	this.verifySessionRoute = function () {
		var path = $location.path();
		var sessionData;

	   try {

	      if (sessionMode == 'cookies') {
	         sessionData = angular.fromJson($cookies.get(nameData));
	      } else {
	         sessionData = angular.fromJson(localStorage.getItem(nameData));
	      }

	   } catch (e) {
	      //session data is a string separate by commas (old way)
	      if (typeof $cookies.get(nameData) !== 'undefined') {

	         var userData = $cookies.get(nameData).split(',');
	         sessionData = {
	            name: userData[0].replace('+', ' '),
	            email: userData[2],
	            id: userData[3],
	            userLevel: userData[4]
	         }

	      } else {
	         sessionData = undefined;
	      }
	   }


		if(typeof sessionData === 'undefined' || sessionData === null) {
			sessionData = null;
		}

		switch(path) {
         case '/view2':
      		var defer =  $q.defer();
				if (sessionData === null) {
					/*trigger event route change error*/
	            return $q.reject('noSession');
	         }
         break;
      }

 	};

})

.service('authService', function ($http,$q , ENV_REST ) {

	var that = this;
	this.config = {
		urlToActions: ENV_REST.requestUrl,
		errorHandler : function(err) {
			console.log(err);
		},
		startLoadingIndicator : undefined,
		stopLoadingIndicator : undefined
	}

	this.setLoadingIndicators = function(notificators) {
		this.config.startLoadingIndicator = notificators.startLoadingIndicator;
		this.config.stopLoadingIndicator = notificators.stopLoadingIndicator;
	};

	this.setAdditionalOptions = function(promise) {
		promise.error(this.config.errorHandler);

		if ( typeof this.config.startLoadingIndicator !== 'undefined') {
			this.config.startLoadingIndicator();
		};

		if (typeof this.config.stopLoadingIndicator !== 'undefined') {
			promise.finally(function() {
				that.config.stopLoadingIndicator();
			});
		};
		return promise;
	}

	this.loginExternal = function(data) {

		var dataToSend = {};

		if(typeof data.modeMd5 !== 'undefined') {
			dataToSend = {
				credentials : data,
				modeMd5: data.modeMd5
			};
		} else {
			dataToSend = {
				credentials : data
			};
		}

		var wandering = jQuery.ajax({
   		data: dataToSend,
	    	url: ENV_REST.loginCrossDomain,
	    	async: true,
		    /*jsonpCallback: '',			*/
	    	contentType: "application/json",
	    	dataType: 'jsonp',
 	    	success: function(json) {

	    	},
	    	error: function(e) {

		   }
	   });

	   /*var experiencity = jQuery.ajax({
   		data: {
				credentials : data
			},
	    	url: 'http://' + this.config.ENV + 'experiencity.co/php/loginCrossDomain.php',
	    	async: true,
	    	contentType: "application/json",
	    	dataType: 'jsonp',
 	    	success: function(json) {

	    	},
	    	error: function(e) {

		   }
		});*/

		//jQuery.when(wandering, experiencity);
	};

	this.registerExternal = function(data) {
		var dataParams = {
			callback : 'JSON_CALLBACK',
			email : data.email,
			password : data.password,
			username : data.username
		};
		return $http.jsonp('http://' + this.config.ENV + 'experiencity.co/php/createUserCrossDomain.php', {
			params: dataParams
		});
	};

	this.register = function(data) {
		var dataParams = {
			action: 'registerUser',
			parameters: data
		};
		var promise = $http.post(this.config.urlToActions, dataParams, {timeout: 10000});
		return this.setAdditionalOptions(promise);
	};

	this.login = function(data) {
		var dataParams = {
			action: 'loginUser',
			parameters: data
		};
		var promise = $http.post(this.config.urlToActions, dataParams, {timeout: 10000});
		return this.setAdditionalOptions(promise);
	};

	this.isUserLoggedOnWandering = function(data) {
		var dataParams = {
			callback : 'JSON_CALLBACK',
			email : data.email,
			username : data.userName
		};
		var promise = $http.jsonp(ENV_REST.checkSessionWanderingUrl,{
			params: dataParams,
			timeout: 10000
		});
		return this.setAdditionalOptions(promise);
	};

	this.logout = function() {

	};

})

//We are inserting Facebook service because is a independently module
//all these services only work online
.service('authSocialService',function($http,Facebook,ENV_REST) {

	var that = this;
	var that = this;
	this.config = {
	   urlToActions: ENV_REST.requestUrl,
	   urlToAPI: ENV_REST.wanderingAPIUrl,
		errorHandler : function(err) {
			console.log(err);
		},
		startLoadingIndicator : undefined,
		stopLoadingIndicator : undefined
	}

	/*In the app module should be this code :
	.config(['FacebookProvider', function (FacebookProvider) {
    FacebookProvider.init(APPID);
	}])*/

	this.facebook = Facebook;
	this.facebookPermissions = {
		scope: 'email'
	};

	this.setLoadingIndicators = function(notificators) {
		this.config.startLoadingIndicator = notificators.startLoadingIndicator;
		this.config.stopLoadingIndicator = notificators.stopLoadingIndicator;
	};

	this.setAdditionalOptions = function(promise) {
		promise.error(this.config.errorHandler);

		if ( typeof this.config.startLoadingIndicator !== 'undefined') {
			this.config.startLoadingIndicator();
		};

		if (typeof this.config.stopLoadingIndicator !== 'undefined') {
			promise.finally(function() {
				that.config.stopLoadingIndicator();
			});
		};
		return promise;
	}

	this.facebookAuthentication = function(callback) {

      this.facebook.getLoginStatus(function (response) {

         if (response.status == 'connected') {
            that.facebookMe(callback);
         } else {
            that.facebook.login(function (response) {

            	if (response.status == 'connected') {
            		that.facebookMe(callback);
            	}

            },that.facebookPermissions );
         }

      });

   };

   this.facebookMe = function(callback) {
   	that.facebook.api('/me',function(response) {
      	if (typeof callback !== 'undefined') {
      		callback(response);
      	};
      });
	};

	this.googleAuthentication =  function(callback) {

		this.google.checkStatus(function (response) {
			if (response !== null && response.status.google_logged_in == true ) {
				that.googleMe(callback);
			} else {
				that.google.login(function (response) {
             	if (response) {
            		that.googleMe(callback);
             	}
            });
			}
		});

	};

	this.googleMe = function(callback) {
	 	this.google.me(function (response) {
			if (typeof callback !== 'undefined') {
      		callback(response);
      	};
      });
	};

	this.socialLoginAndRegister = function(data) {
		var promise = $http.post(this.config.urlToAPI + '/login/social', data, { timeout: 10000 });
		return this.setAdditionalOptions(promise);
	};

	this.existUserOnWandering = function(data) {
		var dataParams = {
			action: 'existUser',
			parameters: data
		};
		var promise = $http.post(this.config.urlToActions, dataParams, {timeout: 10000});
		return this.setAdditionalOptions(promise);
	}

	/*touse google in the controller put this authSocialService.google.init(clientid,apikey)
	google developer console JAVASCRIPT ORIGINS add the domains and google plus API "On"*/

	this.google = new function () {
		"use strict";
		var googleReady = false;
		var defaults = {
		   'immediate': true,
		   'response_type': 'token',
		   'cookiepolicy': 'single_host_origin',
		   'requestvisibleactions': 'http://schemas.google.com/AddActivity',
		   'scope': ['https://www.googleapis.com/auth/plus.login', 'https://www.googleapis.com/auth/userinfo.email']
		};

		this.clientId = "";
		this.apiKey = "";

		(function () {
		   var gscript = document.createElement('script');
		   gscript.type = 'text/javascript';
		   gscript.async = true;
		   gscript.src = 'https://apis.google.com/js/client:plusone.js';
		   gscript.onload = function () {
		       googleReady = true;
		   };
		   var s = document.getElementsByTagName('script')[0];
		   s.parentNode.insertBefore(gscript, s);
		})();

		this.init = function (clientId, apiKey) {
		   this.clientId = clientId;
		   this.apiKey = apiKey;
		   defaults.client_id = this.clientId;
		};

		this.isReady = function () {
		   return googleReady;
		};

		this.checkStatus = function (callback) {
		   defaults.immediate = true;
		   gapi.auth.authorize(defaults, callback);
		};

		this.login = function (callback) {
		   defaults.immediate = false;
		   gapi.auth.authorize(defaults, callback);
		};

		this.me = function (callback) {
		   gapi.client.setApiKey(this.apiKey);
		   gapi.client.load('plus', 'v1', function () {
		       var request = gapi.client.plus.people.get({
		           'userId': 'me'
		       });
		       request.execute(callback);
		   });
		};
	};

})

.value('version', '0.1');
