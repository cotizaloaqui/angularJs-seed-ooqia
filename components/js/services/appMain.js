'use strict';

// General app service
angular.module('appMainModule', [
	'requestModule',
])

.service('appMain', ['$rootScope', '$location', '$timeout', '$q', 'toaster', 'request', 'modal', 'sessionService', 'googleMapsSrv',
	function($rootScope, $location, $timeout, $q, toaster, request, modal, sessionService, googleMapsSrv) {
	var that = this;
   this.scope = {};
   this.featureSrv = {};

	this.showLoadingAjax = false;
	this.notify = toaster;
	this.request = request;
   this.viewFullHeight = true;
	// To allow showLoadingAjax value in request
	// and show the loader
	this.request.appMain = this;
	this.modal = modal;
	this.google = {
		googleMapsSrv: googleMapsSrv
	};

   this.slidePanel = {
      showRight: false,
      showLeft: false,
      showBottom: false,
      showTop: false
   };

	this.userSession;
	// Add all shared methods between features here
	this.sharedLogic = {};

   this.init = function(scope,currentFeatureSrv) {
      this.scope = scope;
      this.scope.appMain = this;
      this.scope.featureSrv = currentFeatureSrv;
      this.scope.featureSrv.appMain = this;
   };

   this.showTopBar = function (state) {
      var topBar = jQuery('.navbar');
      if (state) {
         topBar.show();
      } else {
         topBar.hide();
      }
   };

   this.showFooter = function (state) {
      var footer = jQuery('.footer');
      if (state) {
         footer.show();
      } else {
         footer.hide();
      }
   };

	// Create map example
	this.createMap = function (mapContainerId) {
		var deferred = $q.defer();

		this.googleMaps.map = new this.googleMaps.service(document.querySelector('#' + mapContainerId), this.scope);

     	if (this.googleMaps.map.googleMapsAPILoaded != true) {

        	this.googleMaps.map.googleMapsAPILoaded.then(function () {
           	that.googleMaps.map.loadMap().then(function () {
           		deferred.resolve();
           	});
        });

     	} else {
     		// Wait view on angular
     		$timeout(function () {
            that.mapSrv.loadMap().then(function () {
            	deferred.resolve();
            });
         }, 400);

     	}

     	return deferred.promise;

	};

	 this.createSession = function () {
      if (typeof this.userSession !== 'undefined') {
         sessionService.create({
            id: this.userSession.id,
            email: this.userSession.email,
            userName: this.userSession.userName,
            userLevel: this.userSession.userlevel
         });
         this.userSession = sessionService.getSession();

      } else {
         this.notify.pop("error", 'Error trying to login', 'Please try again..', 5000);
      }
   };

   $rootScope.$on("$routeChangeError", function (event, current, previous, rejection) {
      switch (rejection) {
         case "noSession":
            $location.path('/login');
            that.notify.pop("info", 'please log in to continue', '', 5000);
            break;
         case "userNonAuthorized":
            $location.path('/login');
            that.notify.pop("error", 'You are not allow to access this section', '', 5000);
            break;
      }
   });

}])


.service('modal',['$modal', function ($modal) {
   var that = this;
   this.modalSrv = $modal;
   this.modalInstance = undefined;
   this.modalOptions = {
      templateUrl: '',
      windowClass: '',
      backdrop: true,
      keyboard: false,
      scope: undefined
   };

   this.open = function (options, customClass, callBackOnClose) {
      this.close();

      if (typeof customClass !== 'undefined') {
         this.modalOptions.windowClass = customClass;
      }

      if (typeof options === 'undefined') {
         this.modalInstance = this.modalSrv.open(this.modalOptions);
      } else {
         this.modalInstance = this.modalSrv.open(options);
      }

      this.modalInstance.result.then(function () {
         if (typeof callBackOnClose !== 'undefined') {
            callBackOnClose();
         }
      });
      return this.modalInstance;
   };

   this.close = function () {
      try {
         if (typeof this.modalInstance !== 'undefined') {
            this.modalInstance.close();
            this.modalInstance = undefined;
         }
      } catch (err) {
         console.log('We could not close the modal');
      }
   };
}])

.service('mediaLibrary', ['$timeout', '$interval', '$q', '$http', 'sessionService', 'modal', 'toaster', function ($timeout,$interval, $q, $http, sessionService, modal, toaster) {
   var that = this
   this.actionUrl = 'http://' + window.location.host + '/php/library/mediaLibrary.php';
   this.services = {};
   this.notify = toaster;
   this.scope;
   this.promiseMediaSelected;
   this.timerCreateUploader;

   this.mediaModalOptions = {
      templateUrl: 'components/html/modals/mediaLibrary.html',
      windowClass: 'mediaGalleryModal',
      backdrop: true,
      keyboard: false,
      scope: undefined
   };

   this.userMedia = {
      images: [],
      videos: []
   };

   this.user = sessionService.getSession();

   this.successGetMediaRequest = function (items, type) {
      if (items.length > 0) {
         items.reverse();//last first
         if (type == 'images') {
            that.userMedia.images = items;
         } else {
            that.userMedia.videos = items;
         }

      } else {
         that.notify.pop("info", '', type + ' not found, gallery is empty..', 5000);
      }
      that.scope.appMain.showLoadingAjax = false;
   };

   this.open = function (currentScope,itemType) {
      this.mediaModalOptions.scope = currentScope;
      this.scope = currentScope;
      this.promiseMediaSelected = $q.defer();

      var modalInstance = modal.open(this.mediaModalOptions, 'mediaLibrary');

      modalInstance.opened.then(function () {
         that.scope.appMain.showLoadingAjax = true;
         switch(itemType) {
            case 'images':
               that.services.getUserPictures().then(function (response) {
                  that.successGetMediaRequest(response.data.images, 'images');
               });
            break;
            case 'videos':
               that.services.getUserVideos().then(function (response) {
                  that.successGetMediaRequest(response.data.videos, 'videos');
               });
            break;
            case 'both':
               var promises = {};
               promises.images = that.services.getUserPictures();
               promises.videos = that.services.getUserVideos();

               $q.all(promises).then(function (responseAll) {
                  that.successGetMediaRequest(responseAll.images.data.images, 'images');
                  that.successGetMediaRequest(responseAll.videos.data.videos, 'videos');
               });

            break;   
         }

         that.createUploader();
      });

      return this.promiseMediaSelected.promise;
   };

   this.selectMedia = function(item,type) {
      this.promiseMediaSelected.notify({
         type: type,
         item: item
      });
      modal.close();
   };

   /*********uploader *****************/
   this.callbackSuccessUpload = undefined; //callback to execute any logic after the upload
   this.imageExtensions = ['jpg', 'jpeg', 'png', 'gif'];
   this.videoExtensions = ['3gp', 'flv', 'mp2', 'mpa', 'mpe', 'mpeg', 'mpg', 'mpv2', 'mp4',
  'mov', 'qt', 'lsf', 'lsx', 'asf', 'asr', 'asx', 'avi', 'movie'];
   this.actionUploaderUrl = 'php/library/mediaLibrary.php';

   this.createUploader = function () {

      function initUploader() {
         if ($('.qq-uploader').length == 0) {
            that.setupPluginUploadFile();            

            $('#captionImageUploaded_withoutCaption').change(function () {
               if ($(this).is(":checked")) {
                  $('.qq-uploader').show();
               } else {
                  $('.qq-uploader').hide();
               }
            });
         }
      }

      this.timerCreateUploader = $interval(function () {
         if (document.getElementById('uploadMedia') !== null) {
            $interval.cancel(that.timerCreateUploader);
            initUploader();
         }
      }, 500);
   };

   this.setupPluginUploadFile = function () {      
      
      var uploader = new qq.FileUploader({
         element: document.getElementById('uploadMedia'),
         action: this.actionUrl + '?action=UploadFile&user=' + this.user.id,
         multiple: false,
         allowedExtensions: this.imageExtensions.concat(this.videoExtensions),
         onSubmit: function (id, fileName) {            

            var extension = fileName.split('.').pop();
            extension = extension.toLowerCase();
            //check what kind of file is 
            if (_.contains(that.videoExtensions, extension)) {
               // attribute data-only="image" in the html, to upload only video
               if (typeof uploader._element.dataset.only !== 'undefined') {
                  if (uploader._element.dataset.only == 'image') {
                     alert('You can only upload images.');
                     return false;
                  }
               }

               var titleVideo = $('.media-library .media-library-titleVideo');
               titleVideo.show();
               //if title video is empty
               if (titleVideo.find('input').val() == "") {
                  titleVideo.find('input').parent().css('border', '2px dotted red');
                  console.log('cancel upload video');
                  return false;
               } else {
                  uploader.setParams({
                     title: titleVideo.find('input').val(),
                     typeMedia: 'video'
                  });
                  titleVideo.find('input').val('');
                  titleVideo.hide();
               }
            } else {
               if (typeof uploader._element.dataset.only !== 'undefined') {

                  if (uploader._element.dataset.only == 'video') {
                     alert('You can only upload videos.');
                     return false;
                  } else {
                     uploader.setParams({ typeMedia: 'image' });
                  }
               } else {
                  uploader.setParams({ typeMedia: 'image' });
               }
            }
         },
         onComplete: function (id, fileName, responseJSON) {            
            if (responseJSON.success) {
               if (responseJSON.type == 'image') {
                  if (typeof that.callbackSuccessUpload !== 'undefined') {
                     that.callbackSuccessUpload(responseJSON);
                  } else {
                     that.services.getUserPictures().then(function (response) {
                        that.successGetMediaRequest(response.data.images, 'images');
                     });
                  }

               } else {
                  $('#uploadMedia ul.qq-upload-list').append('<li><span class="qq-upload-spinner"></span>processing video...</li>');
                  setTimeout(function () {
                     that.checkVideoStatus(responseJSON.id);
                  }, 2000);
               }
            }
         }
      });
      

      $('.media-library .media-library-titleVideo input').on('blur keyup', function () {
         if ($(this).val() != "") {
            $(this).parent().css('border', '1px solid #aaa');
         }
         else {
            $(this).parent().css('border', '2px dotted red');
         }
      });
   };

   this.checkVideoStatus = function (videoId) {
      
      var promise = this.services.checkVideoStatus({ "id": videoId });
      promise.then(function (data) {

         switch (data.data.state) {
            case 'ok':
               //remove msg processing
               $('#uploadMedia ul.qq-upload-list .qq-upload-spinner').parent().html(data.data.desc);
               data.data.type = 'video';

               if (typeof that.callbackSuccessUpload !== 'undefined') {
                  that.callbackSuccessUpload(data.data);
               } else {
                  that.services.getUserVideos().then(function (response) {
                     that.successGetMediaRequest(response.data.videos, 'videos');
                  });
               }
               break;
            case 'processing':
               $timeout(function () {
                  that.checkVideoStatus(data.data.id);
               }, 2000);
               break;
            default:
               $('#uploadMedia ul.qq-upload-list .qq-upload-spinner').parent().html(data.data.desc);
         }

      });
   }


   this.successUpload = function (data) {

      if (data.success == true) {
         if (data.type == "image") {
            that.services.getUserPictures().then(function (response) {
               that.successGetMediaRequest(response.data.images, 'images');
            });
         } else {
            that.services.getUserVideos().then(function (response) {
               that.successGetMediaRequest(response.data.videos, 'videos');
            });
         }
      }
   };

   /*******services DB*********/
   this.services.getUserPictures = function () {
      var promise = $http.get(that.actionUrl, {
         params: {
            "action": "getUserPictures", "parameters": { id: that.user.id }
         },
         timeout: 10000
      });
      
      return promise;
   };

   this.services.getUserVideos = function () {

      var promise = $http.get(that.actionUrl, {
         params: {
            "action": "getUserVideos", "parameters": { id: that.user.id }
         },
         timeout: 10000
      });
      
      return promise;
   };

   this.services.checkVideoStatus = function (parameters) {
      var promise = $http.get(that.actionUrl, {
         params: { "action": "checkVideoStatus", "parameters": parameters },
         timeout: 10000
      });      
      return promise;
   };

   //this.services.deleteImage = function (paramters) {

   //   var promise = $http.get(that.actionUrl, {
   //      params: { "action": "deleteImage", "parameters": paramters },
   //      timeout: 10000
   //   });     
   //   return promise;
   //};   

   //this.services.deleteVideo = function (paramters) {
   //   var promise = $http.get(that.actionUrl, {
   //      params: { "action": "deleteVideo", "parameters": paramters },
   //      timeout: 10000
   //   });      
   //   return promise;
   //};

   //this.services.checkUserPermission = function (paramters) {
   //   var promise = $http.get(that.actionUrl, {
   //      params: { "action": "checkUserPermission", "parameters": paramters },
   //      timeout: 10000
   //   });

   //   return promise;
   //};
   

}]);