/** HTTP service will be placed here **/
'use strict';

angular.module('requestModule', [])

.service('request', ['$http', 'ENV_REST', function($http, ENV_REST) {
	var that = this;
	this.appMain;
   this.config = {
      url: ENV_REST.requestUrl,
      errorHandler: function (err) {
         console.log(err);
      }
   };

   // Show/hide Loader in requests
   this.setAdditionalOptions = function (promise) {
      promise.error(this.config.errorHandler);
      that.appMain.showLoadingAjax = true;

      promise.finally(function () {
         that.appMain.showLoadingAjax = false;
      });

      return promise;
   };

    this.movies = {
   	get: function (data) {
	      var dataParams = {
	         action: 'getAll',
	         parameters: data
	      };

	      that.config.url = 'https://yts.to/api/v2/list_movies.json?sort=seeds&limit=15';
	      var promise = $http.get(that.config.url, { params: dataParams, timeout: 10000 });
	      return that.setAdditionalOptions(promise);
   	}
   };

}]);