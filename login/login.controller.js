'use strict';

angular.module('loginModule', [])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/login', {
    templateUrl: 'login/login.html',
    controller: 'Login'
  });
}])

.controller('Login', ['$scope', '$timeout', '$routeParams', '$location', 'appMain', 'authRestService', 'authService', 'authSocialService',
   function($scope, $timeout, $routeParams, $location, appMain, authRestService, authService, authSocialService) {
	$scope.appMain = appMain;
	appMain.scope = $scope;
   $scope.showRegister = false;

  $scope.loginData = {
      email: '',
      password: ''
   };

   $scope.registerData = {
      nick: '',
      email: '',
      password: ''
   };
   $scope.socialData = {};
   $scope.messages = {
      REGISTER_SUCCESFULLY: 'Register successfull',
      REGISTER_FAILED: 'An error has occurred while registering. Please try again..'
   };

   authSocialService.google.init(
      "890799716100-spdlgo27ud71qug2jnb711j2s471n6gq.apps.googleusercontent.com",
      "AIzaSyAzDb-9uCgchMBg4kzbuaDicD5GbN8MnoM"
   );

   $scope.loginSucess = function () {
      $scope.appMain.createSession();
      if (typeof $routeParams.redirecto === 'undefined') {
         $location.path('/view1'); // Redirect url here
      } else {
         $location.path(decodeURIComponent($routeParams.redirecto));
      }
   };

   $scope.login = function () {
      $scope.appMain.showLoadingAjax = true;

      $scope.appMain.userSession = authRestService.resource.login({
         email: $scope.loginForm.email.$viewValue,
         password: $scope.loginForm.password.$viewValue,
      });

      $scope.appMain.userSession.$promise.then(function (response) {

         authService.loginExternal({
            email: $scope.loginForm.email.$viewValue,
            password: $scope.loginForm.password.$viewValue
         });
         $scope.loginSucess(response.data.userlevel);

      }).catch(function (err) {
         $scope.appMain.notify.pop("error", err.data.message, '', 5000);
      }).finally(function () {
         $scope.appMain.showLoadingAjax = false;
      });
   };

   $scope.register = function () {

      if ($scope.registerForm.password.$viewValue != $scope.registerForm.password2.$viewValue) {
         return;
      }

      if ($scope.registerForm.$valid && $scope.registerForm.$dirty) {
         $scope.appMain.showLoadingAjax = true;
         $scope.appMain.userSession = authRestService.resource.register({
            userName: $scope.registerForm.nick.$viewValue,
            email: $scope.registerForm.email.$viewValue,
            password: $scope.registerForm.password.$viewValue,
            birthDate: '0000-00-00'
         });

         $scope.appMain.userSession.$promise.then(function (response) {

            $scope.loginSucess();
            // authService.loginExternal({
            //    email: $scope.registerForm.email.$viewValue,
            //    password: $scope.registerForm.password.$viewValue
            // });

            $scope.showRegister = false;
           $scope.appMain.notify.pop("info", '', $scope.messages.REGISTER_SUCCESFULLY, 5000);
         }).catch(function (err) {
           $scope.appMain.notify.pop("error", '', $scope.messages.REGISTER_FAILED, 5000);
         }).finally(function () {
           $scope.appMain.showLoadingAjax = false;
         });
      }

   };

      $scope.facebookAuthentication = function () {
         authSocialService.facebookAuthentication(function (response) {
            $scope.socialData = response;
            $scope.socialData.firstName = response.first_name;
            $scope.socialData.lastName = response.last_name;
            $scope.socialData.profileUrl = response.link;
            $scope.socialData.provider = 'Facebook';

            authSocialService.socialLoginAndRegister($scope.socialData).then(function (response) {

               if (response.data.status == 'ok') {
                  $scope.appMain.userSession.id = response.data.data.id;
                  $scope.appMain.userSession.email = response.data.data.email;
                  $scope.appMain.userSession.userName = response.data.data.username;
                  $scope.appMain.userSession.userlevel = response.data.data.user_level;
                  $scope.loginSucess();

                  // authService.loginExternal({
                  //    email: response.data.data.email,
                  //    password: response.data.data.guid,
                  //    modeMd5: false
                  // });

               } else {
                  $scope.appMain.notify.pop("error", response.data.msg, '', 5000);
               }

            });
         });
      };

      $scope.googleAuthentication = function () {
         authSocialService.googleAuthentication(function (response) {
            $scope.socialData = response;
            $scope.socialData.email = response.emails[0].value;
            $scope.socialData.firstName = response.name.givenName;
            $scope.socialData.lastName = response.name.familyName;
            $scope.socialData.name = response.displayName;
            $scope.socialData.profileUrl = response.url;
            $scope.socialData.provider = 'Google';
            $scope.socialData = _.omit($scope.socialData, ['ageRange', 'emails', 'result', 'image', 'etag', 'kind']);

            authSocialService.socialLoginAndRegister($scope.socialData).then(function (response) {

               if (response.data.status == 'ok') {
                  $scope.appMain.userSession.id = response.data.data.id;
                  $scope.appMain.userSession.email = response.data.data.email;
                  $scope.appMain.userSession.userName = response.data.data.username;
                  $scope.appMain.userSession.userlevel = response.data.data.user_level;
                  $scope.loginSucess();

                  authService.loginExternal({
                     email: response.data.data.email,
                     password: response.data.data.guid,
                     modeMd5: false
                  });

               } else {
                  $scope.appMain.notify.pop("error", response.data.msg, '', 5000);
               }

            });
         });
      };

	$scope.$on('$viewContentLoaded', function () {

   });

   $scope.$on("$destroy", function () {

   });

}]);